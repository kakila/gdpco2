# GDP vs. CO2

A simple causal analysis of the data reported in [Nordborg's blog](https://nordborg.ch/2018/08/12/sustainable-growth-is-an-oxymoron)

The code uses the Gaussian likelihood score (see pp. 64 in [Elements of Causal Inference](https://www.dropbox.com/s/o4345krw428kyld/11283.pdf?dl=1)) to probe if GDP causes CO2, or the other way around.

The results favor the casual direction GDP casues CO2:

| Causal direction | Log Marginal Likelihood |
|------------------|-------------------------|
| GDP --> CO2      | -794.35                 |
| CO2 --> GDP      | -1618.73                |

The analysis involves a non-parametric non-linear regression (same for both directions) and a simple calculation on the empirical residuals.

Check the code for details.

To run the code yourself you need [GNU Octave 4.4.0 or later](https://www.gnu.org/software/octave/), the [gpml](https://bitbucket.org/KaKiLa/gpml) package, and the [Octave Forge signal](https://octave.sourceforge.io/signal/) package and.
You can install the packages (in linux) doing

```
pkg install https://bitbucket.org/KaKiLa/gpml/downloads/gpml-4.2.0.tar.gz
pkg -forge install control signal
```

without modifications it should produce the following plots

## Regression results
![regression](Regression.png)

## Histogram of residuals
![histogram](HistResiduals.png)

## Autocorrelation functions of residuals
![histogram](AutocorrelationRes.png)
