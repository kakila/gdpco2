## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-09-18

pkg load gpml
pkg load signal

tmp = load ('co2-gdp-summer.csv');
gdp = tmp(:,2); # USD
co2 = tmp(:,3); # ton / 1e3
clear tmp

[X avgX stdX] = zscore ([gdp co2]);
Y = [co2 gdp];

meanf = {@meanSum, {@meanLinear, @meanConst}};
covf  = {@covMaterniso, 3};
likf  = @likGauss;

hyp0 = struct ('cov', [0; 0], 'mean', [0; 0], 'lik', 0);

args = {@infExact, meanf, covf, likf};

evalc ('h1 = minimize_minfunc (hyp0, @gp, -100, args{:}, X(:,1), Y(:,1));');
evalc ('h2 = minimize_minfunc (hyp0, @gp, -100, args{:}, X(:,2), Y(:,2));');

nlml1 = gp(h1, args{:}, X(:,1), Y(:,1));
nlml2 = gp(h2, args{:}, X(:,2), Y(:,2));
printf ('Log Marginal Likelihood\n');
printf ('GDP --> CO2: %.2f\n', -nlml1);
printf ('CO2 --> GDP: %.2f\n', -nlml2);
fflush (stdout);

res1 = Y(:,1) - gp (h1, args{:}, X(:,1), Y(:,1), X(:,1));
res2 = Y(:,2) - gp (h2, args{:}, X(:,2), Y(:,2), X(:,2));
[acf1 lag1] = xcorr (res1, 20, 'coeff');
[acf2 lag2] = xcorr (res2, 20, 'coeff');

figure (1), clf
  subplot (2, 1, 1)
  hold on
  x = linspace (min(X(:,1)), max(X(:,1)), 100).';
  [y vary] = gp (h1, args{:}, X(:,1), Y(:,1), x);
  h = shadowplot (x * stdX(1) + avgX(1), y, 1.96 * sqrt (vary));
  plot (gdp, co2, 'ko')
  hold off
  axis tight
  xlabel ('GDP [USD]')
  ylabel ('CO2 [ton / 1e3]')
  legend (h.line.center, {sprintf('GDP --> CO2: %d', -nlml1)}, 'location', 'northwest')

  subplot (2, 1, 2)
  x = linspace (min(X(:,2)), max(X(:,2)), 100).';
  [y vary] = gp (h2, args{:}, X(:,2), Y(:,2), x);
  h = shadowplot (x * stdX(2) + avgX(2), y, 1.96 * sqrt (vary));
  plot (co2,  gdp, 'ko')
  hold off
  axis tight
  ylabel ('GDP [USD]')
  xlabel ('CO2 [ton / 1e3]')
  legend (h.line.center, sprintf('CO2 --> GDP: %d', -nlml2), 'location', 'northwest')

figure (2), clf
  subplot (2, 1, 1)
  %plot (gdp, res, 'ko')
  hist (res1)
  axis tight
  %xlabel ('GDP [USD]')
  xlabel ('Residuals')
  legend ({sprintf('GDP --> CO2: %d', -nlml1)}, 'location', 'northwest')

  subplot (2, 1, 2)
  %plot (co2, res, 'ko')
  hist (res2)
  axis tight
  %xlabel ('CO2 [ton / 1e3]')
  xlabel ('Residuals')
  legend (sprintf('CO2 --> GDP: %d', -nlml2), 'location', 'northwest')

figure (3), clf
  plot (lag1, acf1, lag2, acf2)
  axis tight
  line (axis()(1:2), 0);
  grid on
  xlabel ('Lag')
  ylabel ('Correlation coefficient')
  hl = legend ('GDP --> CO2', 'CO2 --> GDP');
  set (hl, 'color', 'w');
